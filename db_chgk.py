# -*- coding: utf-8 -*-
import sqlite3


def create_db(db_name):
    conn = sqlite3.connect(db_name)
    cursor = conn.cursor()

    cursor.execute('DROP TABLE chgk_users; ')
    cursor.execute('DROP TABLE chgk_score;')

    cursor.execute("""CREATE TABLE chgk_users
                      ( id integer PRIMARY KEY , username text NULL, first_name text NULL, last_name text NULL, time_regist DATETIME  DEFAULT CURRENT_TIMESTAMP NOT NULL
                       )
                  """)

    cursor.execute("""CREATE TABLE chgk_score
                      ( id integer PRIMARY KEY , score integer NULL
                       )
                   """)
    conn.close()


def create_db_questions(db_name):
    conn = sqlite3.connect(db_name)
    cursor = conn.cursor()
    cursor.execute("""CREATE TABLE chgk_questions
                          ( id integer PRIMARY KEY AUTOINCREMENT NOT NULL, vopros text NULL, otvet text NULL, komment text NULL, time_questions DATETIME  DEFAULT CURRENT_TIMESTAMP NOT NULL
                           )
                      """)
    conn.close()


#create_db_questions('chgk_base.db')

def insert_user_db(db_name, id, username, first_name, last_name):
    conn = sqlite3.connect(db_name)
    cursor = conn.cursor()
    cursor.execute("""SELECT * from chgk_users where id = '%s'
    """ % id)
    if cursor.fetchone() is None:

        cursor.execute("""INSERT INTO chgk_users (id, username, first_name, last_name) VALUES 
                              ('%s','%s','%s','%s'
                              )
        """ % (id, username, first_name, last_name))

        cursor.execute("""INSERT INTO chgk_score  VALUES 
                              ('%s','%s'
                              )
        """ % (id, 0))

        result = "%s %s вы зарегистрированны, удачи!" % (first_name, last_name)
    else:
        result = "%s %s вы уже зарегистрированны!" % (first_name, last_name)
    conn.commit()
    conn.close()
    return result


def proverka_user_db(db_name, id, level):
    conn = sqlite3.connect(db_name)
    cursor = conn.cursor()
    cursor.execute("""SELECT * from chgk_users where id = '%s'
        """ % id)
    if cursor.fetchone() is None:
        result = "вы не зарегистрированны, пройдите регистарцию /register"
    else:
        if level == 0:
            result = "вам начисленно три очка"
            sc = 3
        elif level == 1:
            result = "вам начисленно два очка"
            sc = 2
        elif level == 2:
            result = "вам начисленно одно очко"
            sc = 1
        cursor.execute("""UPDATE chgk_score set score = score + '%s' where id = '%s' 
        """ % (sc, id))

    conn.commit()
    conn.close()
    return result


def insert_questions_in_db(db_name, vopros, otvet, komment):
    conn = sqlite3.connect(db_name)
    cursor = conn.cursor()
    cursor.execute("""SELECT * from chgk_questions where vopros = '%s'
    """ % vopros)
    if cursor.fetchone() is None:

        cursor.execute("""INSERT INTO chgk_questions (vopros, otvet, komment) VALUES 
                              ('%s','%s','%s'
                              )
        """ % (vopros, otvet, komment))
    conn.commit()
    conn.close()


def my_db_score(db_name, id):
    conn = sqlite3.connect(db_name)
    cursor = conn.cursor()
    cursor.execute("""SELECT * from chgk_users where id = '%s'
            """ % id)
    if cursor.fetchone() is None:
        result = "вы не зарегистрированны, пройдите регистарцию /register"
    else:
        cursor.execute("""select username, score from chgk_score inner join chgk_users on chgk_users.id=chgk_score.id where chgk_score.id= '%s'
        """ % id)
        query = cursor.fetchone()
        result = '%s у вас %s очков' % (query[0], query[1])
    conn.close()
    return result


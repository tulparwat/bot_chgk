# -*- coding: utf-8 -*-
import requests
from bs4 import BeautifulSoup
from lxml import html
import random


def vopros_chgk():
    url = 'https://db.chgk.info/random/limit1'
    page = requests.get(url).text
    tree = html.fromstring(page)
    otvet = tree.xpath(
        "//body/div[@id='bg1']/div[@id='bg2']/div[@id='body_bg']/div/div/div[@id='body_left']/div[@id='body_right']/div[@id='middlecontainer']/div[@id='wrapper']/div/div/div/div[@id='main']/div/div//p")[
        1]
    soup = BeautifulSoup(page, "html.parser")
    div_vopros = soup.find("div", {'class': 'random_question'}).text
    vopr_nach = div_vopros.find('Вопрос 1:') + 9
    vopr_konec = div_vopros.find('Ответ:') - 6
    koment_nach = div_vopros.find('Комментарий:')
    vopros = div_vopros[vopr_nach:vopr_konec]
    if koment_nach == -1:
        komment = 'Коментария нету'
    else:
        komment = div_vopros[koment_nach:]
    return vopros, otvet.text_content()[11:], komment


def podskaz(slovo, level, level_old=0, podskzkz_old=''):
    slovo = slovo.strip()
    kolsimpodsk_new = round(len(slovo) * level)
    kolsimpodsk_old = round(len(slovo) * level_old)
    kolsimpodsk = kolsimpodsk_new - kolsimpodsk_old
    bukvi = []
    bukvi_old = []
    podskazka = list(slovo)

    if podskzkz_old != '':
        for i in range(0, len(podskzkz_old)):
            if podskzkz_old[i] != '*':
                bukvi_old.append(i)
    if kolsimpodsk > 0:
        if bukvi_old:
            for i in range(0, len(slovo)):
                bukvi.append(i)
            bukvi = sorted(bukvi_old + random.sample(list(set(bukvi) - set(bukvi_old)), kolsimpodsk))
            for i in range(0, len(slovo)):
                if i in bukvi:
                    podskazka[i] = slovo[i]
                else:
                    podskazka[i] = '*'
        else:
            for i in range(0, len(slovo)):
                bukvi.append(i)
            bukvi = sorted(random.sample(bukvi, kolsimpodsk))

            for i in range(0, len(slovo)):
                if i in bukvi:
                    podskazka[i] = slovo[i]
                else:
                    podskazka[i] = '*'
    slovo = ''.join(podskazka)
    return slovo


def is_number(str):
    try:
        float(str)
        return True
    except ValueError:
        return False

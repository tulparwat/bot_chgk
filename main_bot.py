# -*- coding: utf-8 -*-
import time
import sys
from main_function import vopros_chgk, podskaz, is_number
import telepot
from fuzzywuzzy import fuzz
from db_chgk import insert_user_db, proverka_user_db, my_db_score, insert_questions_in_db
import config


if len(sys.argv) > 1:
    rejim_raboti = sys.argv[1]
else:
    rejim_raboti = 'normal'

vopros = otvet = komment = time_s = ''
podskazka = 'Нет подсказки задай сначало вопрос /question'
level = 0

def handle(msg):
    content_type, chat_type, chat_id = telepot.glance(msg)
    print(content_type, chat_type, chat_id)
    global vopros, otvet, komment, podskazka, time_s, level
    if (content_type == 'text'):
        command = msg['text']
        print('Got command: %s' % command)

        if '/start' in command:
            bot.sendMessage(chat_id, "Чтобы получить вопрос введите /question, для получения ответа /answer, но для начало пройди регистарцию /register")

        if '/news' in command:
            bot.sendMessage(chat_id, "Последнее обновление было 30 октября, теперь вопросы попадают в БД и у бота появился режим тех. работ")

        if '/question' in command:

            if vopros == '':
                vopros, otvet, komment = vopros_chgk()
                podskazka = ''
                level = 0
                time_s = 200
                insert_questions_in_db('chgk_base.db', vopros.replace('\n', ' '), otvet.replace('\n', ' '), komment)
            bot.sendMessage(chat_id, vopros.replace('\n', ' '))
        if '/myscore' in command:
            users = bot.getUpdates()
            user = my_db_score('chgk.db', users[0]["message"]["from"]["id"])
            bot.sendMessage(users[0]["message"]["from"]["id"], user)


        if '/answer' in command:
            if otvet != '':
                bot.sendMessage(chat_id, otvet.replace('\n', ' '))
                if komment != '':
                    bot.sendMessage(chat_id, komment)
                bot.sendMessage(chat_id, 'За этот вопрос баллов ни кому не начисленно')
                vopros, otvet, komment = vopros_chgk()
                podskazka = ''
                level = 0
                time_s = 200
                bot.sendMessage(chat_id, vopros.replace('\n', ' '))
                insert_questions_in_db('chgk_base.db', vopros.replace('\n', ' '), otvet.replace('\n', ' '), komment)



            else:
                bot.sendMessage(chat_id, 'Нажмите /question')
        else:
            if is_number(time_s) is True:
                if time_s > 0:
                    if fuzz.token_sort_ratio(command, otvet) > 80:
                        users = bot.getUpdates()
                        user = proverka_user_db('chgk.db', users[0]["message"]["from"]["id"], level)
                        if user == "":
                            bot.sendMessage(chat_id, 'Верно! %s' % user)
                            bot.sendMessage(chat_id, otvet)
                            if komment != '':
                                bot.sendMessage(chat_id, komment)
                        else:
                            bot.sendMessage(chat_id, "Верно!  %s" % user)
                            if komment != '':
                                bot.sendMessage(chat_id, komment)
                        vopros = otvet = komment = time_s = ''
                        podskazka = 'Нет подсказки задай сначало вопрос /question'
                    else:
                        print(fuzz.token_sort_ratio(command, otvet))
                else:
                    bot.sendMessage(chat_id, 'Время вышло правильный ответ:')
                    bot.sendMessage(chat_id, otvet)
                    if komment != '':
                        bot.sendMessage(chat_id, komment)
                    vopros = otvet = komment = time_s = ''
                    podskazka = 'Нет подсказки задай сначало вопрос /question'

            if '/hint' in command:
                if podskazka == '' or (level >0 and level<2):
                    if level == 0:
                        podskazka = podskaz(otvet, 0.3)
                        level += 1
                    elif level > 0:
                        podskazka = podskaz(otvet, 0.4, 0.3, podskazka)
                        level += 1

                bot.sendMessage(chat_id, podskazka)
                if level == 1:
                    bot.sendMessage(chat_id, 'Активированна подсказка 1 уровня, количество очков будет уменьшенно')
                elif level == 2:
                    bot.sendMessage(chat_id, 'Активированна подсказка 2 уровня, количество очков будет уменьшенно')
            if '/register' in command:
                users = bot.getUpdates()
                print(users)
                if 'username' in users:
                    username = users[0]["message"]["from"]["username"]
                else:
                    username = users[0]["message"]["from"]["first_name"]
                if 'last_name' in users:
                    last_name = users[0]["message"]["from"]["last_name"]
                else:
                    last_name = ''
                user = insert_user_db('chgk.db', users[0]["message"]["from"]["id"],
                                      username,
                                      users[0]["message"]["from"]["first_name"],
                                      last_name)
                bot.sendMessage(chat_id, user)

def handle_renovation_work(msg):
    content_type, chat_type, chat_id = telepot.glance(msg)
    print(content_type, chat_type, chat_id)
    if (content_type == 'text'):
        command = msg['text']
        print('Got command: %s' % command)

        if '/' in command:
            bot.sendMessage(chat_id, "БОТ на ремонте обращаться к Артемке он все наладит, наверное")


bot = telepot.Bot(config.token)

if rejim_raboti == 'normal':
    bot.message_loop(handle)
elif rejim_raboti == 'renovation':
    bot.message_loop(handle_renovation_work)

while 1:
    time.sleep(5)
    if is_number(time_s) is True:
        time_s = time_s - 5
    else:
        vopros = otvet = komment = time_s = ''
        podskazka = 'Нет подсказки задай сначало вопрос /question'
        level = 0

